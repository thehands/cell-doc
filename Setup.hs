module Main where

import Distribution.Simple
import Distribution.Simple.Setup (BuildFlags)
import Distribution.PackageDescription (HookedBuildInfo, emptyHookedBuildInfo)
import System.Process (runCommand,  waitForProcess )

main :: IO ()
main = defaultMainWithHooks $ simpleUserHooks { preBuild = genCSS }

genCSS :: Args -> BuildFlags -> IO HookedBuildInfo
genCSS _ _ = do
    waitForProcess =<< runCommand "cell snaplets/templates | tee resources/style.css"
    return emptyHookedBuildInfo
