module Main where

import Snap.Snaplet
import Snap.Snaplet.Heist
import Snap.Util.FileServe (serveDirectoryWith, simpleDirectoryConfig)
import Snap.Http.Server.Config (defaultConfig)
import Control.Lens (lens, Lens', (&), (.~))
import Data.Map.Syntax ((##))
import Heist
import Heist.Splices

data CellDoc = CellDoc { _heist :: Snaplet (Heist CellDoc) }

heist :: Lens' CellDoc (Snaplet (Heist CellDoc))
heist = lens _heist (\app v -> app { _heist = v })

cellDocInit :: SnapletInit CellDoc CellDoc
cellDocInit = makeSnaplet "cell" "Cell documentation site." Nothing $ do
    addRoutes [ ("resources", serveDirectoryWith simpleDirectoryConfig "resources")
              , ("", heistServe)
              ]
    let heistConfig = emptyHeistConfig
          & hcLoadTimeSplices .~ do
              applyTag ## applyImpl
              bindTag ## bindImpl
              ignoreTag ## ignoreImpl
              htmlTag ## htmlImpl
              markdownTag ## markdownSplice
          & hcNamespace       .~ ""
    h <- nestSnaplet "heist" heist $ nameSnaplet "templates" $ heistInit' "" heistConfig
    return $ CellDoc h


instance HasHeist CellDoc where heistLens = subSnaplet heist

main :: IO ()
main = serveSnaplet defaultConfig cellDocInit
