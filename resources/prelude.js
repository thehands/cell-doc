/* Time */

function getTimeFormatted(t) {
  if (t == undefined) {
    var time = new Date();
  } else {
    var time = new Date(parseInt(t, 10));
  }
  var hours = time.getHours() % 12 || 12;
  var minutes = time.getMinutes();
  if (1200 > (time.getHours() + minutes) < 2400) {
    var ext = "p";
  } else {
    var ext = "a";
  }
  minutes = (minutes<10)?'0'+minutes:minutes;
  return hours + ":" + minutes + ext;
}

/* Cookie */

function getCookies() {
  var rawCookies = document.cookie.split(/ ;| ,|=/);
  var cookies = new Object();
  for (var i=0; i < rawCookies.length;i=i + 2) {
    cookies[rawCookies[i]] = rawCookies[i+1];
  }
  return Object.freeze(cookies);
}

function getCookie(name) {
  var match = document.cookie.match(RegExp('(?:^|;\\s*)' + name + '=([^;]*)'));
  return match ? match[1] : null;
}

/* HTTP */

var getData = function(type, url, callback) {
  var x = new XMLHttpRequest();
  x.open("get", url);
  x.responseType = type;
  x.onload = function() {
    if (x.status == 200) {
      callback(x.response);
    } else {
      console.log(x.statusText);
    }
  };
    x.send();
};

/* Search */

function getStrDist(a, b) {
  var al = a.length;
  var bl = b.length;
  if (al === 0) return bl;
  if (bl === 0) return al;
  var matrix = [];
  // increment along the first column of each row
  var i;
  for (i = 0; i <= bl; i++) {
    matrix[i] = [i];
  }
  // increment each column in the first row
  var j;
  for (j = 0; j <= al; j++) {
    matrix[0][j] = j;
  }
  // Fill in the rest of the matrix
  for (i = 1; i <= bl; i++) {
    for (j = 1; j <= al; j++) {
      if (b.charAt(i-1) == a.charAt(j-1)) {
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
  return matrix[bl][al];
};

function search(needle, haystack, props, softness) {
  softness = typeof softness !== 'undefined' ? softness : 2;
  var results = [];
  var passed;
  var score;
  var thing;
  for (var i=0; i < haystack.length; i++) {
    passed = false;
    for (var j=0; !passed && j < props.length; j++) {
      if (haystack[i][props[j]]) {
        var tokens = (haystack[i][props[j]]).split(' ');
        for (var k=0; k < tokens.length; k++) {
          s = getStrDist(needle, tokens[k])
          if (s <= softness) {
            thing = haystack[i];
            thing.score = s;
            results.push(thing);
            passed=true;break;
          }
        }
      }
    }
  }
  return results.sort(function(a, b){return a.score - b.score});
};

/* Conversion */

function mapToArray(map) {
  var key;
  var array = [];
  for (key in map) {
    map[key]["key"] = key
    array.push(map[key]);
  }
  return array;
};

function arrayToMap(array) {
  var map = new Object();
  var key;
  for (var i=0; i < array.length; ++i) {
    key = array[i].key;
    delete array[i].key;
    map[key] = array[i];
  }
  return map;
};
