function displayDoc(listNode, css, style) {
  for (var i = 0; i < css.length; ++i) {
    var section = document.createElement("section");
    section.className = style.section;
    var article = document.createElement("article");
    article.className = style.article;
    var h = document.createElement("h3");
    h.className = style.heading;
    h.innerText = css[i].name;
    var desc = document.createElement("p");
    desc.className = style.desc;
    var paragraph = document.createElement("p");
    paragraph.className = style.paragraph;
    var quarc = document.createElement("code");
    quarc.className = style.quarc;
    var qtemplate = document.createElement("code");
    qtemplate.className = style.template;
    var quarcName;
    var key = css[i]["key"]
    if (css[i].template.search("$") != -1) {
      if (css[i].values && css[i].values[0]) {
        var sig = css[i].values[0];
        quarcName = css[i].key + "(" + sig +") ";
      } else {
        quarcName = css[i].key + "(<Args>) ";
      }
    } else {
      quarcName = css[i].key + "() ";
    }
    quarc.textContent = quarcName;
    qtemplate.textContent = (css[i].template).replace(" $", " $");
    desc.innerText = css[i].desc;
    paragraph.appendChild(quarc);
    paragraph.appendChild(qtemplate);
    article.appendChild(paragraph);
    if (css[i].aliases) {
      var aliases = css[i].aliases;
      for (abbr in aliases) {
        var paragraph = document.createElement("p");
        paragraph.className = style.paragraph;
        var quarc = document.createElement("code");
        quarc.className = style.quarc;
        var qtemplate = document.createElement("code");
        qtemplate.className = style.class;
        var quarcName = css[i].key + "(" + abbr + ") "
        var template = css[i].template.substring(0, css[i].template.search(" ")) + ' ' + aliases[abbr];
        quarc.textContent = quarcName;
        qtemplate.textContent = template;
        paragraph.appendChild(quarc);
        paragraph.appendChild(qtemplate);
        article.appendChild(paragraph)
      }
    }
    section.appendChild(h);
    section.appendChild(desc);
    section.appendChild(article);
    listNode.appendChild(section);
  }
}

function searchDoc(listNode, css, style) {
  return function(e) {
    if (e.keyCode === 13) {
      while (listNode.hasChildNodes()) {
          listNode.removeChild(listNode.lastChild);
      }
      terms = e.target.value.split(" ");
      if (terms.length > 1) {
        var results = [];
        for (var i=0; i < terms.length; ++i) {
          results = results.concat(search(terms[i], css, ["key", "name"], 0));
        }
      } else {
        var results = (search(e.target.value, css, ["key", "name", "template"])).sort(function(a, b){
          return a.score - b.score
        });
      }
      displayDoc(listNode, results, style);
    }
  }
}
