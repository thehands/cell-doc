#!/bin/sh

{ # Wrap to prevent partial downlaod execution.

echo "Attempting to retrieve cell.."
if curl -Ss "cellcss.com/resources/cell" > /tmp/cell;then
	if grep -iq "<!DOCTYPE html>" /tmp/cell ;then
		echo "Error: Cell not found on server."
		exit
	fi
	chmod +x /tmp/cell
	sudo mv /tmp/cell /usr/bin/cell
	echo "Installed Successfully!"
	exit
fi
echo "Error: Failed to retrive cell from server."

} # End wrapping

