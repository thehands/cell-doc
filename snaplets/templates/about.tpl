<apply template="_layout">
  <section class="Ff(robotoRegular) Fw(n)">
    <h1 class="Fw(n)">Web design in real time</h1>
    
    <p>
      With Quarc CSS you only generate css you need instead of (mis)managing css you might need. No bloat. No frameworks.<a href="/about">Learn more about quarc --></a>
    </p>
    
    <p>
      "Quarcs" are functional, single-purpose classes. 
      Know exactly how your css is affecting your markup.
    </p>
    
    <p>
      Quarc began as a fork of the excellent <a href="http://acss.io">Atomic CSS</a>. We decided to rewrite the engine from scratch, in haskell.
    </p>
    
    <p>
      "Patterns" are where quarcs come alive: based on the excellent <a href="http://tachyons.io/components/">Tachyons component library</a> they are examples how to combine quarcs to make interface components and layouts. 
    </p>
    
    <p>
      Read our <a href="/docs">Docs</a> to <a href="/docs/getting-started">get started</a>; learn about layout strategies like <a href="/docs/responsive-web-design">responsive web design</a>, and <a href="/docs/flex">flex</a> (with out of the box prefix support);
    </p>
  </section>

</apply>
