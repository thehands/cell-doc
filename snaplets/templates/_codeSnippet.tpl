<head>
  <script src="/resources/electrical.js"></script>
</head>

<h1 class="Ff(robotoRegular) Fw(n) Fz(1.5rem)"><patternName/></h1>
<pre data-el="codeSnippet copySource" class="eDiode Bd(solid,1px,gray) Bgc(white-20) C(rgba(0,0,0,.7)) Ff(robotoMonoRegular) Fz(1em) H(16rem) Lih(1.15) Ov(s) P(0)">
</pre>
