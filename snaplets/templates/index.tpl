<apply template="_layout">
<head>
  <script src="/resources/quarc-doc.js"></script>
  <script src="/resources/prelude.js"></script>
  <script defer>
    document.addEventListener("DOMContentLoaded", function(e){
      var style = new Object();
      /* Start Style. */
      style.section = "Pb(1.25em)";
      style.article = "";
      style.heading = "Ff(gidoleRegular) Fw(b) Fz(1.25rem) Fz(1.5rem)-md Lts(.05em) Mb(1rem) P(0)";
      style.desc = "C(navy) Ff(gidoleRegular) Fz(1.25rem) M(0) P(0)"
      style.paragraph = "M(0) Py(.15rem)";
      style.quarc = "Fz(1rem) Fz(1.15rem)-md Fz(1.25rem)-lg C(rgb(22,22,33)) Ff(robotoMonoRegular) Fw(n)";
      style.template = "Fz(1rem) Fz(1.15rem)-md Fz(1.25rem)-lg C(rgb(136,129,144)) Ff(robotoMonoRegular) Fw(n)";
      /* End Style. */
      var list = document.getElementById("list");
      getData("json", "/resources/css.min.json", function(css) {
        css = mapToArray(css);
        displayDoc(list, css, style);
        document.getElementById("search").onkeydown = searchDoc(list, css, style);
      })
    });
  </script>
</head>

<div class="P(1.125rem) Bgc(black-80) M(0px,0px,5px,0px)">
  <p class="C(yellow-washed) Ff(gidoleRegular) Fz(1.25rem) Maw(35em)"><strong class="Ff(robotoMonoBold) Fw(n)">cells</strong> are single-purpose css classes you write directly in your markup and cell generates your stylesheet. For fast-as-thought web design without specificity wars, bloat, or frameworks.
  <a href="/about" class="C(grey-5) Ff(gidoleRegular) Td(n) Td(u):h Tt(u)">Learn more</a> or,
  <a href="/docs/quick-start" class="C(grey-5) Ff(gidoleRegular) Td(n) Td(u):h Tt(u)">Quick start</a>
  </p>
</div>

<div id="list" class="P(1.125rem)">
</div>

</apply>
