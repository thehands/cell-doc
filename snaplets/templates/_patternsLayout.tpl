<apply template="_layout">
    <h1 class="Ff(robotoMedium) Fw(n)"><a href="/patterns/patternsIndex" class="C(rgb(136,129,144)) C(#ff5252):h Td(n)">patterns</a> <span class="C(#777)"> / </span> <patternName/></h1>
    <apply-content/>
</apply>
