<apply template="_docsLayout">
  <bind tag="docName">Quick Start</bind>
  <p class="Fs(i)">"There are no shortcuts... Only different ways to pay." -- A wise person</p>
</apply>
