<apply template="_docsLayout">
  <h2 class="Ff(karlaBold) Fw(n) Mb(0)">Basics</h2>
  <ul class="Ff(robotoMonoRegular) Lh(1.5em) List(n) Mt(0) P(0)">
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="quick-start">Quick Start</a></li>
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="responsive-web-design">Responsive Web Design</a></li>
  </ul>
</apply>

