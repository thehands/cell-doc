<!DOCTYPE html>
<html lang="en" class="H(100%) M(0)">

  <head>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>Cell Reference</title>
    <link rel="stylesheet" href="/resources/style.css"/>
    <link rel="stylesheet" href="/resources/fonts/webfonts.css"/>
  </head>

  <body class="Bgc(white) Bxz(bb) D(f) Fld(c) Fz(1rem) H(100%) M(0) Mih(100%)">
    <header class="Bdb(d,1px,#000) Bdt(solid,4px,grey-3) M(0) P(0) Bgc(#f4f4f4)">
      <input type="search" id="search" placeholder="Type a css or class name here..."
        class="_rInput() Bgc(white) C(gray) C(red):f D(ib) Ff(robotoMonoRegular) Fw(n) Fz(1rem) Miw(30%) Trsp(color) Trsdu(0.3s) Ai(c) Bdr(d,1px,#aaa)@md P(.25rem)">
      <div class="D(i) P(.25rem) Pr(.75rem)">
        <bind tag="linkStyle">Fz(1.5rem) Fw(n) Ff(gidoleRegular) C(#000) C(#777):h Lts(.1em) Td(n) Trsp(color) Trsdu(.3s)</bind>
        <a href="/" class="${linkStyle}">
          cells</a>
        <a href="/patterns/patternsIndex" class="${linkStyle}">
          patterns</a>
        <a href="/docs/docsIndex" class="${linkStyle}">
          docs</a>
        <a href="/about" class="${linkStyle}">
          about</a>
      </div>
    </header>
    
    <main class="Flx(a) Ovx(h) Ovy(s)">
      <apply-content/>
    </main>
  </body>
</html>
