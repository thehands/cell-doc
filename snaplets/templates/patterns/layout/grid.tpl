<bind tag="patternName">Grid</bind>

<apply template="_patternsLayout">

<div>
  <p><code>Flx($0,$1,$2)</code> is the shorthand for (default values shown):</p>
  <table class="Ff(m)">
    <tr>
      <td>flex-grow:</td>
      <td>0</td>
    </tr>
    <tr>
      <td>flex-shrink:</td>    
      <td>1</td>    
    </tr>
    <tr>
      <td>flex-basis:</td>    
      <td>auto</td>
    </tr>
  </table>
</div>

<h3 class="Ff(robotoBold) Fw(n) Fz(1.2em)">Fixed Columns</h3>
<div class="Maw(90%) Mx(a) My(3rem) Bd(dotted,1px,grey-7)">

  <!-- grid -->
  <bind tag="gutter">P(.5rem) Bd(dotted,1px,grey-7)</bind>
  <bind tag="cell">Bd(dotted,1px,solid) Bgc(grey-9) Ff(robotoMonoRegular) Py(.889rem) Ta(c)</bind>

  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
  </div>

  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(2) ${gutter}">
      <div class="${cell}">Flx(2)</div>
    </div>
  </div>

  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(3) ${gutter}">
      <div class="${cell}">Flx(3)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(4) ${gutter}">
      <div class="${cell}">Flx(4)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(5) ${gutter}">
      <div class="${cell}">Flx(5)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(6) ${gutter}">
      <div class="${cell}">Flx(6)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(7) ${gutter}">
      <div class="${cell}">Flx(7)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(8) ${gutter}">
      <div class="${cell}">Flx(8)</div>
    </div>
  </div>
  
  <div class="D(f) Bxz(bb)">
    <div class="Flx(1) ${gutter}">
      <div class="${cell}">Flx(1)</div>
    </div>
    <div class="Flx(9) ${gutter}">
      <div class="${cell}">Flx(9)</div>
    </div>
  </div>
  
</div>
  
<h3 class="Ff(robotoBold) Fw(n) Fz(1.2em)">Responsive Columns</h3>
<div class="Maw(90%) Mx(a) My(3rem) Bd(dotted,1px,grey-7)">

  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
  </div>

  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(2)@md ${gutter}">
      <div class="${cell}">Flx(2)@md</div>
    </div>
  </div>

  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(3)@md ${gutter}">
      <div class="${cell}">Flx(3)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(4)@md ${gutter}">
      <div class="${cell}">Flx(4)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(5)@md ${gutter}">
      <div class="${cell}">Flx(5)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(6)@md ${gutter}">
      <div class="${cell}">Flx(6)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(7)@md ${gutter}">
      <div class="${cell}">Flx(7)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(8)@md ${gutter}">
      <div class="${cell}">Flx(8)@md</div>
    </div>
  </div>
  
  <div class="D(f)@md Bxz(bb)">
    <div class="Flx(1)@md ${gutter}">
      <div class="${cell}">Flx(1)@md</div>
    </div>
    <div class="Flx(9)@md ${gutter}">
      <div class="${cell}">Flx(9)@md</div>
    </div>
  </div>
  
</div>

<h3 class="Ff(robotoBold) Fw(n) Fz(1.2em)">Two Columns - Collapsing</h3>
<div class="Maw(90%) Mx(a) My(3rem) Bd(dotted,1px,grey-7)">
  <div class="eTrigger" data-el="codeSnippet">
    
    <div class="D(f)@md">
      <div class="Flx(1)@md ${gutter}">
        <div class="${cell}">Flx(1)@md</div>
      </div>
      <div class="Flx(1)@md ${gutter}">
        <div class="${cell}">Flx(1)@md</div>
      </div>
      
    </div>
  </div>

</div>

<div class="Maw(90%) Mx(a)">
  <apply template="_codeSnippet"></apply>
</div>

</apply>
