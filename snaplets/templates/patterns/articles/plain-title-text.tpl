<bind tag="patternName">Plain Title Text</bind>

<apply template="_patternsLayout">

<div class="eTrigger" data-el="codeSnippet">
  <!-- Plain Title Text -->
  <article class="P(1rem) P(4rem)--md">
    <h1 class="">Title</h1>
    <p class="Maw(30em) Lh(1.5)">
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
      tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
      vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
      no sea takimata sanctus est Lorem ipsum dolor sit amet.
    </p>
    <p class="Maw(30em) Lh(1.5)">
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
      tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
      vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
      no sea takimata sanctus est Lorem ipsum dolor sit amet.
    </p>
  </article>
</div>

<apply template="_codeSnippet"></apply>

</apply>
