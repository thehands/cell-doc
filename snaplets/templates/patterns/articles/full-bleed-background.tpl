<bind tag="patternName">Full Bleed Background</bind>

<apply template="_patternsLayout">
  
<div class="eTrigger" data-el="codeSnippet">
  <!-- Full Bleed Background Article -->
  <article class="D(f) Bg(url(/resources/fbb.jpg),nr,c,c,f) Bgz(cv)">
    <div class="Bgc(#fff) C(rgba(0,0,0,.7)) Ff(serif) Fl(start) Fz(1.5rem) Maw(20em) P(1rem) P(2rem)--md">
      <header class="BdB(solid_1px_rgba(0,0,0,.7)) Py(2rem)">
        <h3 class="Ff(sans) Fw(700) Fz(2.25rem) Lh(1.25) Lts(.1em) Mt(0) Mb(1rem) Tt(u)">Prasat Suor Prat</h3>
        <h4 class="Fz(1.5rem) Fw(400) Fs(i) Lh(1.25) Mt(0)">Siem Reap, Cambodia</h4>
      </header>
      <section class="Pt(4rem) Pb(2rem)">
        <p class="Ff(serif) Ff(1.25rem) Lh(1.5) Maw(30rem) Mt(0)">
          Prasat Suor Prat is a series of twelve towers spanne
          north to south lining the eastern side of royal square in Angkor Thom,
          near the town of Siem Reap, Cambodia. The towers are made from rugged
          laterite and sandstone. The towers are located right in front of
          Terrace of the Elephants and Terrace of the Leper King, flanking the
          start of the road leading east to the Victory Gate, on either side of
          which they are symmetrically arranged. Their function remains unknown.
        </p>
      </section>
    </div>
  </article>
</div>

<apply template="_codeSnippet"></apply>

</apply>
