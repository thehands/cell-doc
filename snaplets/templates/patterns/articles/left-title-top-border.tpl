<bind tag="patternName">Left Title Top Border</bind>

<apply template="_patternsLayout">

<div class="eTrigger" data-el="codeSnippet">
  <!-- Left Title Top Border -->
  <article class="D(f)--md">
    <header class="Pend(2rem)--md W(50%)--md">
      <h1 class="BdT(solid_1px) Bdw(.25rem) Fw(900) Fz(2.25rem) Lh(1.25) Mb(1rem) Mt(0) Pt(1rem)">
        On Typography
      </h1>
      <h2 class="C(#555) Fw(400) Fz(1.5rem) Lh(1.25)">
        An excerpt from the Form of the Book by Jan Tschichold
      </h2>
      <time class="C(#777) Ff(sans-serif) Fz(.875rem) Lts(.1em) Tt(u)">Sometime before 1967</time>
    </header>
    <div class="W(50%)--md">
      <p class="Ff(sans-serif) Fz(.875rem) Lh(1.5) Maw(30em) Mt(0)--md">
        TYPOGRAPHY, even when poorly executed, can never be taken for granted;
        nor is it ever accidental. Indeed, beauti- fully typeset pages are always
        the result of long experience. Now and then they even attain the rank of
        great artistic achievement. But the art of typesetting stands apart from
        ex- pressive artwork, because the appeal is not limited to a small
        circle. It is open to everyone's critical judgment, and nowhere does this
        judgment carry more weight. Typography that can- not be read by everybody
        is useless. Even for someone who constantly ponders matters of
        readability and legibility, it is difficult to determine whether
        something can be read with ease, but the average reader will rebel at
        once when the type is too small or otherwise irritates the eye; both are
        signs of a certain illegibility already.
      </p>
      <p class="Ff(sans-serif) Fz(.875rem) Lh(1.5) Maw(30em)">
        All typography consists of letters. These appear either in the form of a
        smoothly running sentence or as an assembly of lines, which may even have
        contrasting shapes. Good typog- raphy begins, and this is no minor
        matter, with the typeset- ting of a single line of text in a book or a
        newspaper. Using exactly the same typeface, it is possible to create either
        a pleasant line, easily read, or an onerous one. Spacing, if it is too wide
        or too compressed, will spoil almost any typeface.
      </p>
    </div>
  </article>
</div>

<apply template="_codeSnippet"></apply>
  
</apply>
