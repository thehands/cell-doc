<bind tag="patternName">Large Title Text</bind>

<apply template="_patternsLayout">

<div class="eTrigger" data-el="codeSnippet">
  <!-- Large Title Text -->
  <article class="P(1rem) P(4rem)--md">
    <h1 class="Fz(2.25rem) Fz(5rem)--md Mb(2rem)">9.5.1 If the text will be read on the screen, design it for that medium.</h1>
    <p class="C(#777) Fz(.875rem) Mb(2rem) Tt(u) Lts(.1em)">By Robert Bringhurst</p>
    <p class="Fz(1.25rem) Fz(1.5rem)--md Maw(30em) Lh(1.5)">
      Like a forest or a garden or a field, an honest page of letters can absorb &ndash;
      and will repay &ndash; as much attention as it is given. Much type now, however, is
      composed not for the page but for the screen of a computer. That screen can be
      alive with flowing color, but the best computer monitors have dismal resolution
      (about 130 dpi: one fifth the current norm for laser printers and roughly 5% of
      the norm for professional digital typesetting). When the text is crudely
      rendered, the eye goes looking for distraction, which the screen is all too
      able to provide.
    </p>
    <p class="Fz(1.25rem) Fz(1.5rem)--md Maw(30em) Lh(1.5)">
      The screen mimics the sky, not the earth. It bombards the eye with light
      instead of waiting to repay the gift of vision. It is not simultaneously
      restful and lively, like a field full of flowers, or the face of a thinking
      human being, or a well-made typographic page. And we read the screen the way
      we read the sky: in quick sweeps, guessing at the weather from the changing
      shapes of clouds, or like astronomers, in magnified small bits, examining
      details. We look to it for clues and revelations more than wisdom. This makes
      it an attractive place for advertising and dogmatizing, but not so good a
      place for thoughtful text.
    </p>
    <p class="Fz(1.25rem) Fz(1.5rem)--md Maw(30em) Lh(1.5)">
      The screen, in other words, is a reading environment even more fugitive than
      the newspaper. Intricate long sentences full of unfamiliar words stand little
      chance. At text size, subtle and delicate letterforms stand little chance as
      well. Superscripts and subscripts, footnotes, endnotes, sidenotes disappear. In
      the harsh light and coarse resolution of the screen such accessories are
      difficult to see; what is worse, they dispel the essential illusion of speed.
      so the links and jumps of hypertext replace them. All the subtexts then can be
      the same size and readers are at liberty to skip from text to text like
      children switching channels on tv. When reading takes this form, sentences and
      letterforms retreat to blunt simplicity. Forms bred on newsprint and signage
      are most likely to survive.
    </p>
  </article>
</div>

<apply template="_codeSnippet"></apply>

</apply>
