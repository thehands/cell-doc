<bind tag="patternName">Left Title</bind>

<apply template="_patternsLayout">

<div class="eTrigger" data-el="codeSnippet">
  <!-- Left Title -->
  <article class="D(f)--md Px(1rem) Px(4rem)--md Py(4rem)">
    <header class="Pstart(2em)--md W(50%)--md">
      <h1 class="Lh(1.25) Mb(1rem) Mt(0)">Clay in a Potter's Hand</h1>
      <time class="C(#777) Fz(.875rem) Lts(.1em) Tt(u)">Jan Tschichold</time>
    </header>
    <div class="Ff(ss)">
      <p class="Lh(1.5) Maw(30em) Mt(2rem) Mt(0)--md">
        PERFECT typography is more a science than an art. Mastery of the trade is
        indispensable, but it isn't everything. Unerring taste, the hallmark of
        perfection, rests also upon a clear understanding of the laws of harmonious
        design. As a rule, impeccable taste springs partly from inborn sensitivity:
        from feeling. But feelings remain rather unproductive unless they can inspire a
        secure judgment. Feelings have to mature into knowledge about the consequences
        of formal decisions. For this reason, there are no born masters of typography,
        but self- education may lead in time to mastery.
      </p>
      <p class="Lh(1.5) Maw(30em)">
        It is wrong to say that there is no arguing about taste when it is good taste
        that is in question. We are not born with good taste, nor do we come into this
        world equipped with a real understanding of art. Merely to recognize who or
        what is represented in a picture has little to do with a real under- standing
        of art. Neither has an uninformed opinion about the proportions of Roman
        letters. In any case, arguing is senseless. He who wants to convince has to
        do a better job than others.
      </p>
    </div>
  </article>
</div>
  
<apply template="_codeSnippet"></apply>

</apply>
