<bind tag="patternName">Feature</bind>

<apply template="_patternsLayout">

<div class="eTrigger" data-el="codeSnippet">
  <!-- Feature -->
  <article class="Ff(serif)">
    <div class="H(100vh) D(tb) W(100%) Ta(c) C(#fff) Bgz(cv) Bg(url(/resources/f.jpg),nr,c)">
      <div class="D(tc) Va(m)">
       <header class="C(rgba(255,255,255,.7))">
          <h2 class="Fz(.875rem) Fw(100) Tt(u) Lts(1em) Mb(.5rem) Lh(1.25)">Issue Six</h2>
          <h3 class="Fz(.875rem) Fw(100) Lh(1.25)">Summer MMXVI</h3>
        </header>
        <h1 class="Fz(3rem) Fz(6rem)-md Fw(100) Fs(i) C(rgba(255,255,255,.6))">The Chronicles</h1>
        <blockquote class="Px(0) Mx(0) Mw(30em) Fz(1.25rem) Lh(1.5) Mx(a)">
          <p class="Fw(100) C(rgba(255,255,255,.7))">
            It's the space you put between the notes that make the music.
          </p>
          <cite class="Fz(.875rem) Tt(u) Lts(.1em) Fs(n)">Massimo Vignelli</cite>
        </blockquote>
      </div>
    </div>
    
    <div class="Mx(a) Mw(34em) Fz(1rem) Py(4rem) Lh(1.5) Px(.5rem)">
      <h1 class="Fz(3rem) Lh(1.25)">The repercussion of ugliness is endless</h1>
      <p>
        The choice of paper size is one of the first of any given work to be printed. There are two basic paper size systems in the world: the international A sizes, and the American sizes.
      </p>
      <p>
        The international Standard paper sizes, called the A series, is based
        on a golden rectangle, the divine proportion. It is extremely handsome
        and practical as well. It is adopted by many countries around the world
        and is based on the German DIN metric Standards. The United States uses
        a basic letter size (8 1/2 x 11) of ugly proportions, and results in
        complete chaos with an endless amount of paper sizes. It is a
        by-product of the culture of free enterprise, competition and waste.
        Just another example of the misinterpretations of freedom.
      </p>
      <p>
        These are the basic DIN sizes in mm. for : A0, 841x1189 - A1, 594x841 -
        A2, 420x594 - A3, 297x420 - A4, 210x297 - A5, 148x210 - A6, 105 x148 -
        A7, 74x 105 - A8, 52x74 - A9, 37x52 - A10, 26x37.
      </p>
      <p>
        The A4 is the basic size for stationary. Two thirds of it is a square,
        a nice economical happenstance resulting from the golden rectangle.
        It is one of the reasons we tend to use as much
        as possible the DIN sizes: proportions are always leading to other nice proportions.
      </p>
      <p>
        This does not happen with the American basic size which leads to nothing. I counted 28 different standard sizes in USA!. The only reason we use it is because everybody in USA uses it, all stationary in USA is that size, so are manilla folders! The repercussion of ugliness is endless.
      </p>
    </div>
  </article>
</div>
  
<apply template="_codeSnippet"> </apply>

</apply>
