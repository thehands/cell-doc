<apply template="_patternsLayout">
  <small> 
    <p class="C(#555) Ff(gidoleRegular) Fz(.889em) Maw(35em)"><strong class="Ff(robotoMonoBold) Fw(n)">patterns</strong> are harmonious combinations of <a href="/" class="C(#555) Ff(robotoMonoBold) Td(n) Td(u):h">quarcs</a> that create components and layouts for building web interfaces.
    </p>
  
    <p class="C(#555) Ff(gidoleRegular) Fz(.889em) Maw(35em)">Patterns are where quarcs come alive. The granular control quarcs provide can be, initially, overwhelming; patterns help you think in combinations of quarcs making them an ideal place to begin a new project. 
    </p>
    
    <p class="C(#555) Ff(gidoleRegular) Fz(.889em) Maw(35em)">
      If you're new to quarc css we recommend the
      <a href="/docs/quick-start" class="C(#555) Ff(gidoleRegular) Td(n) Td(u):h Tt(u)">
        Quick start guide 
      </a>
    </p>
  </small>
  
  <h2 class="Ff(gidoleRegular) Fw(n) Mb(0)">Articles</h2>
  <ul class="Ff(robotoMonoRegular) Lh(1.5em) List(n) Mt(0) P(0)">
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="articles/feature">Feature</a></li>
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="articles/full-bleed-background">Full Bleed Background</a></li>
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="articles/large-title-text">Large Title Text</a></li>
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="articles/left-title">Left Title</a></li>
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="articles/left-title-top-border">Left Title Top Border</a></li>
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="articles/plain-title-text">Plain Title Text</a></li>
  </ul>
  
  <h2 class="Ff(gidoleRegular) Fw(n) Mb(0)">Layout</h2>
  <ul class="Ff(robotoMonoRegular) Lh(1.5em) List(n) Mt(0) P(0)">
    <li><a class="C(rgb(22,22,33) Td(n) Td(u):h" href="layout/grid">Grid</a></li>
  </ul>
</apply>
